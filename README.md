RetroDocs - Documentação de Retrocomputação
===========================================

## Índice

[Dump de cartuchos Color Computer](DumpCartuchosCoCo.md)

## Licença

Todo o conteúdo deste repositório está disponível sob a licença [Creative Commons Atribuição Não-Comercial](LICENSE):

**Você tem o direito de:**

**Compartilhar** — copiar e redistribuir o material em qualquer suporte ou formato

**Adaptar** — remixar, transformar, e criar a partir do material

O licenciante não pode revogar estes direitos desde que você respeite os termos da licença.

**De acordo com os termos seguintes:**

**Atribuição** — Você deve dar o crédito apropriado, prover um link para a licença e indicar se mudanças foram feitas. Você deve fazê-lo em qualquer circunstância razoável, mas de nenhuma maneira que sugira que o licenciante apoia você ou o seu uso.

**Não Comercial** — Você não pode usar o material para fins comerciais.

Sem restrições adicionais — Você não pode aplicar termos jurídicos ou medidas de caráter tecnológico que restrinjam legalmente outros de fazerem algo que a licença permita.
