title: Dump de Cartuchos de CoCo

Dump de Cartuchos de CoCo
=========================

Por Claudemir Todo Bom / Licença: [Creative Commons BY-NC](LICENSE)

A transferência do conteúdo da memória de cartuchos do TRS Color Computer pode ser feita lendo diretamente o cartucho utilizando um gravador de EPROM e um cabo adaptador, porém este equipamento não é comum a todos os colecionadores.

O método que eu utilizo faz uso apenas de equipamentos comuns, como o próprio CoCo e seus cabos originais de interface cassette.

[[_TOC_]]

Material Necessário
-------------------

* Color Computer ou compatível
* Cartuchos a serem copiados *
* Fita isolante
* Computador ou notebook (com Linux **)
* Software de gravação de áudio
* Conjunto de ferramentas [Toolshed](https://sourceforge.net/projects/toolshed/) [Site Oficial]
* Emulador capaz de ler arquivo `.wav` e `.dsk` ***
* [Software para o CoCo de transferência de fita para disco](http://www.colorcomputerarchive.com/coco/Disks/Utilities/Copy%20Utilities.zip) [Color Computer Archive]

\* Todos os exemplos deste guia foram feitos com o cartucho "Editor" da Prosoft, por isso os nomes nos comandos refletem isso. Altere adequadamente para o cartucho em questão.

\** Linux é utilizado nestes exemplos, mas qualquer sistema operacional capaz de rodar as ferramentas sugeridas ou até mesmo outras ferramentas que façam o mesmo trabalho podem ser utilizados.

\*** Nos exemplos é utilizado o [MAME](http://www.mame.net/) com a ROM do [CoCo 2](http://www.colorcomputerarchive.com/coco/Emulators/MESS/ROMS/coco2.zip).

Passo 1 - Do cartucho para um arquivo wav
-----------------------------------------

Para execução deste passo é necessário ter o CoCo, com o slot de cartuchos disponível e o cabo de áudio conectado à entrada de line-in ou microfone do PC.

### Prevenindo o autoboot

O pino 8 do conector do cartucho (`!CART`) tem a função de informar o computador da presença do cartucho para que qualquer código presente ali seja executado. Normalmente cartuchos de jogos não possuem os pinos do 1 ao 6.

Uma fita isolante pode ser usada para cobrir o pino 8, normalmente não tem problema em cobrir o 7 (no lado oposto) pois o sinal normalmente não é utilizado, e mesmo que fosse, no caso de cartuchos com outras funções, a ausência desse sinal não impediria a leitura da memória.

![Fita Isolante no e o 7 Cartucho](images/1-prevenir-autoboot.png)

Na imagem o pino exibido é o nr. 7 (`Q`), pois o 8 (`!CART`) está do outro lado. O cartucho utilizado aqui é de um CP-400, atenção então também caso esteja utilizando um Tandy original porque provavelmente você olhará a placa do cartucho pelo lado oposto.

### Gravando o arquivo de áudio

Antes de iniciar, é preciso conectar o pino `MIC` (preto no caso do CP400) do cabo original de cassette na entrada `Line In` ou `MIC` do PC, e também conectar o cartucho já com o pino 8 isolado.

No PC, deve ser iniciado qualquer aplicativo capaz de gravar um arquivo de áudio `.wav` com o mínimo de perdas possível. Na imagem abaixo a tela do Gravador de Som do ambiente Gnome no Linux.

![Gravador de Áudio](images/2-gravador-de-audio.png)

Após ligar o CoCo, a tela inicial deve ser exibida, como se não houvesse cartucho algum no slot, nesta tela deve-se comandar a gravação das posições de memória em fita.

É recomendada a gravação de bancos de 4k individualmente, para facilitar posteriormente a cópia para arquivos binários, antes de executar o comando a seguir, acione a gravação do áudio no PC.

Para um cartucho de 8K, o comando de gravação é o seguinte:

```
CSAVEM"EDITOR1",&HC000,&HCFFF,0:CSAVEM"EDITOR2",&HD000,&HDFFF,0
```

Para cartuchos de 4K apenas a primeira parte é necessária, para cartuchos de 12K ou 16K, adicionar comandos para gravar os blocos `&HE000,&HEFFF` e `&HF000,&HFFFF`, sempre lembrando de salvar os arquivos com nomes diferentes e com no máximo 8 caracteres.

Após a conclusão do comando, pode-se parar a gravação do áudio no PC e salvar o arquivo `.wav`.

**ATENÇÃO:** O arquivo `.wav` resultante não é carregável através do comando `CLOAD`, pois ele contém dados originalmente gravados em ROM. O único objetivo em gerarmos este arquivo é para extrair os dados do CoCo, que serão trabalhados nos passos a seguir até termos um arquivo de cartucho `.ccc` utilizável em emuladores, na CocoSDC ou até como base para a confecção de cartuchos caseiros.

Passo 2 - Do WAV para um DSK
----------------------------

A partir deste passo, todos os trabalhos são executados no PC.

A conversão o arquivo `.wav` para um arquivo `.dsk` justifica-se no tratamento binário do resultado, pois é impossível extrair os bytes armazenados como áudio facilmente através das ferramentas disponíveis.

Para fazermos esta manobra são necessárias as ferramentas do [Toolshed](https://sourceforge.net/projects/toolshed/) e um emulador capaz de trabalhar com o arquivo `.wav` na como entrada de cassette e um arquivo `.dsk` como interface de disco.

É ainda necessário o disco de utilitários de cópia disponível no [Color Computer Archive](http://www.colorcomputerarchive.com/coco/Disks/Utilities/Copy%20Utilities.zip).

**ATENÇÃO:** Todos os comandos estarão sem indicação de caminho propositalmente para razões de clareza, certamente será ser necessário ajustar caminhos para referenciá-los corretamente!

### Criação de um DSK vazio

Antes de converter o arquivo `.wav` para `.dsk` precisamos ter um arquivo `.dsk` vazio, preparado para os trabalhos. Para isso utilizamos o comando `decb dskini` do toolshed, da seguinte forma:

```sh
decb dskini editor.dsk
```

Este comando irá gerar um arquivo chamado `editor.dsk`.

### Lançando o emulador

Tendo o arquivo `copyutils.dsk` com os utilitários de cópia, o arquivo `.wav` gerado a partir do cartucho e o arquivo `.dsk` vazio criado, o comando a seguir lança o MAME utilizando eles e uma ROM de coco2.

```sh
mame coco2 -flop1 copyutils.dsk -flop2 editor.dsk -cass editor.wav
```

### Transferindo da fita pro disco

Após a carga do emulador, digitar os seguintes comandos em sequência para carregar o utilitário de cópia de arquivos:

```
DRIVE0
LOADM"TPTDSK
DRIVE1
EXEC
```

Após a carga e execução, o utilitário irá pedir para posicionar a fita e apertar `ENTER`, basta apertar `ENTER` pois a fita virtual já foi carregada.

O programa irá ler a fita e identificar o primeiro arquivo, no exemplo, `EDITOR1`, dizendo também se tratar de um programa em código de máquina e solicitando se o arquivo deve ser transferido. Basta responder com `Y` e aguardar a transferência.

Uma vez concluída a transferência do arquivo, será solicitado um nome para o arquivo no disco, o ideal é utilizar o mesmo nome do arquivo e adicionar o sufixo `/BIN`.

A imagem a seguir ilustra a interação até este ponto:

![TPTDSK Output](images/3-transferindo-da-fita-pro-disco.png)

Após salvar, o utilitário irá solicitar se deseja reiniciá-lo, deve-se responder `Y` até que todos os blocos de 4K sejam transferidos.

Concluindo esta etapa, já temos o arquivo `.dsk` com o programa em formato binário e é possível encerrar o emulador, restando apenas a última etapa.

**ATENÇÃO:** Da mesma forma que os arquivos armazenados no `.wav`, os arquivos armazenados na imagem de disco `.dsk` não servem para ser carregados através do comando `LOADM`, pelo mesmo motivo de se referirem a uma porção da memória ocupada pela ROM do sistema.

Passo 3 - Do disco pro cartucho (virtual)
-----------------------------------------

A última etapa consiste em extrair os arquivos `/BIN` armazenados dentro da imagem de disco `.dsk` para o sistema de arquivos do PC, posteriormente remover o header e concatená-los em um único arquivo `.ccc` que na verdade é apenas a sequência "crua" dos bytes do cartucho.

### Extraindo os BIN

Novamente contamos com o Toolshed, mais especificamente com o comando `decb copy`, para obter o conteúdo do `.dsk`, através dos seguintes comandos:

```sh
decb copy editor.dsk,EDITOR1/BIN editor1.bin
decb copy editor.dsk,EDITOR2/BIN editor2.bin
```

Tal qual no *Passo 1*, caso o cartucho sendo copiado tenha apenas 4K, o primeiro comando basta, já se tiver 12K ou 16K, devem ser adicionados comandos para copiar os outros blocos.

Após a execução destes comandos, neste exemplo, estarão disponíveis os arquivos `editor1.bin` e `editor2.bin`.

### Removendo o cabeçalho do DECB

O sistema de disco do CoCo coloca em seus arquivos um cabeçalho de 5 bytes, utilizados para indicar o tipo do arquivo, a posição de memória a ser colocado e o ponto de execução. Estas informações não são necessárias na imagem de cartucho, precisamos apenas dos 4096 bytes (4K) de cada arquivo.

Para remover estes bytes, no Linux é possível utilizar o comando `dd`, da seguinte forma:

```sh
dd bs=1 skip=5 count=4096 if=editor1.bin of=editor1.rom
dd bs=1 skip=5 count=4096 if=editor2.bin of=editor2.rom
```

Mais uma vez, é necessário repetir para cada bloco de 4K do cartucho que estamos copiando.

### Juntando tudo

Os blocos de 4K precisam ser unidos em um único arquivo, que neste exemplo totalizará 8K, no linux isso pode ser feito com um único comando simples:

```sh
cat editor1.rom editor2.rom > editor.ccc
```

Passo 4 - testar o arquivo!
---------------------------

Pronto! O seu cartucho agora está corretamente formatado para uso em emuladores e também para o funcionamento na CocoSDC. Para testá-lo no emulador MAME você pode utilizar o seguinte comando:

```sh
mame coco2 -cart editor.ccc
```

E você será brindado com a tela do seu programa dumpeado:

![Teste do Cartucho](images/4-teste-do-cartucho.png)

Passo 5 - talvez não tenha acabado
----------------------------------

É bem provável que você se deu a todo esse trabalho pelo simples motivo de possuir um cartucho que ainda não está presente nos repositórios do [Color Computer Archive](http://www.colorcomputerarchive.com/) ou do [Datassette](https://www.datassette.org), então, faça este favor para os outros CoCo-entusiastas e compartilhe o resultado deste trabalho!
